FROM openjdk:11.0.7-slim-buster
MAINTAINER Nanoo (arnaudlaval33@gmail.com)
ADD target/monitoring-server.jar monitoring-server.jar
ENTRYPOINT ["java","-jar","monitoring-server.jar"]